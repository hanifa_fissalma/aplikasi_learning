<?php
  include 'ceklogin.php';
?>
<?php
include('header.php');
?>
<section id="main-content">
<section class="wrapper">
  <h3><i class="fa fa-mapel"></i> IKUT UJIAN BARU</h3>
  <div class="row">
    <div class="col-md-12">
      <div class="content-panel content-table">
          <?php
              require_once 'db.php';
              $sql="SELECT test.testid, test.testname, test.testdesc, subject.subname, test.duration, test.totalquestions
                    FROM subject, test
                    WHERE test.subid = subject.subid
                    order by testid";
              $stmnt = $dbh->prepare($sql);
              $stmnt->execute();
              if ($stmnt->rowCount() == 0) {
                  echo'Tidak Ada Data';
              } else {
                echo
        '<table id="ambilujian" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>No.</th>
                <th>Nama Ujian</th>
                <th>Deskripsi Ujian</th>
                <th>Mata Pelajaran</th>
                <th>Durasi(dalam menit)</th>
                <th>Total Pertanyaan</th>
                <th class="action" align="center">Mulai Tes</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>No.</th>
                <th>Nama Ujian</th>
                <th>Deskripsi Ujian</th>
                <th>Mata Pelajaran</th>
                <th>Durasi(dalam menit)</th>
                <th>Total Pertanyaan</th>
                <th class="action" align="center">Mulai Tes</th>
              </tr>
            </tfoot>
            <tbody>';
              while ($row = $stmnt->fetch()){
                $testid=$row['testid'];
                $testname=$row['testname'];
                $testdesc=$row['testdesc'];
                $subname=$row['subname'];
                $duration=$row['duration'];
                $totalquestions=$row['totalquestions'];
              echo
              "<tr>
                  <td>$testid</td>
                  <td>$testname</td>
                  <td>$testdesc</td>
                  <td>$subname</td>
                  <td>$duration</td>
                  <td>$totalquestions</td>
                  <td>
                      <a href='kodetes.php?id=" . $row['testid'] . "' title='Mulai Tes' class='btn btn-primary btn-xs'><i class='fa fa-pencil'></i> Mulai Tes</a>
                  </td>
              </tr>";
            }
            '</tbody>
        </table>';
        }
      ?>
      </div>
    </div>
  </div>
</section>
</section>
<script>
  $(document).ready(function() {
    $('#ambilujian').DataTable();
} );
</script>
<?php
include('footer.php'); ?>
