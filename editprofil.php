<?php
  require_once 'db.php';
  include 'ceklogin.php';
?>

<?php
include('header.php');

$query = "SELECT * FROM `student` WHERE stdusername=".$_SESSION['stdusername'];
$stmnt = $dbh->prepare($query);
$stmnt->execute();
$data = $stmnt->fetch();

?>
<section id="main-content">
<section class="wrapper">
  <h3><i class="fa fa-mapel"></i>UBAH PROFIL</h3>
  <div class="row">
    <div class="col-md-12">
      <div class="content-panel content-table">
        <form class="form-add" method='post' action="ubahprofil.php">
              <table class='table table-bordered'>
                  <tr>
                      <td>Nomor Induk Siswa</td>
                      <td><input type='text' name='stdname' class='form-control' value="<?php echo $_SESSION['stdusername'] ?>" readonly></td>
                  </tr>
                  <tr>
                      <td>Nama Lengkap</td>
                      <td><input type='text' name='stdfullname' class='form-control' value="<?php echo $data['stdfullname'] ?>" required></td>
                  </tr>
                  <tr>
                      <td>Password</td>
                      <td><input type='password' name='stdpassword' class='form-control' value="<?php echo $data['stdpassword'] ?>" readonly></td>
                  </tr>
                  <tr>
                      <td>Kelas</td>
                      <td>
                        <select id="level" class="form-control" name="classid" value="<?php echo $data['classid'] ?>" required>
                            <option value="">Silahkan Pilih Kelas</option>
                            <?php
                              require_once 'db.php';
                              $sql="SELECT * FROM `class` ";
                              $stmnt = $dbh->prepare($sql);
                              $stmnt->execute();
                              while ($class = $stmnt->fetch()){
                                $value="";
                                $classid=$class['classid'];
                                $classname=$class['classname'];
                                if($classid == $data['classid']){
                                  $value="selected";
                                }
                                echo "<option value='$classid' ".$value.">$classname</option>";
                              }
                            ?>
                        </select>
                      </td>
                  </tr>
                  <tr>
                      <td>Email</td>
                      <td><input type='text' name='emailid' class='form-control' value="<?php echo $data['emailid'] ?>" required></td>
                  </tr>
                  <tr>
                      <td>Nomor Telepon/HP</td>
                      <td><input type='text' name='contactno' class='form-control' value="<?php echo $data['contactno'] ?>" required></td>
                  </tr>
                  <tr>
                      <td>Alamat</td>
                      <td><input type='text' name='address' class='form-control' value="<?php echo $data['address'] ?>" required></td>
                  </tr>
                  <tr>
                      <td colspan="2">
                          <button type="submit" class="btn btn-primary" name="submit_profile">
                              <span class="glyphicon glyphicon-edit"></span>  Update
                          </button>
                          <a href="#" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Batal</a>
                      </td>
                  </tr>

              </table>
          </form>

      </div>
    </div>
  </div>
</section>
</section>
<?php
include('footer.php'); ?>
