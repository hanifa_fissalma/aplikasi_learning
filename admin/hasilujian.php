<?php
include('header.php');
?>
<section id="main-content">
  <section class="wrapper">
    <h3><i class="fa fa-mapel"></i> Daftar Hasil Ujian</h3>
      <div class="row">
        <div class="col-md-12">
          <div class="content-panel content-table">
            <table class='table table-striped table-advance table-hover'>
            <tr>
               <th class="no">No.</th>
               <th>Nama Ujian</th>
               <th>Waktu Valid</th>
               <th>Nama Mata Pelajaran</th>
               <th>Siswa yang Mengikuti</th>
               <th class="action" align="center">Aksi</th>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <a href="#" title="Detail Hasil Ujian" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></a>
                </td>
            </tr>
          </table>

                        <!-- <?php echo $pages->page_links(); ?> -->
            </div>
         </div>
      </div>
  </section>
</section>
<?php
include('footer.php'); ?>
