<?php
include('header.php');
?>
<section id="main-content">
  <section class="wrapper">
    <h3><i class="fa fa-mapel"></i> Daftar Ujian</h3>
      <div class="row">
        <div class="col-md-12">
          <div class="content-panel content-table">
            <div class="action-button pull-right">
                <a href="#" class="btn btn-large btn-info button-add"><i class="glyphicon glyphicon-plus"></i> &nbsp; Tambah Ujian</a>
            </div>
            <hr>
            <table class='table table-striped table-advance table-hover'>
            <tr>
               <th class="no">No.</th>
               <th>Nama Ujian</th>
               <th>Deskripsi Ujian</th>
               <th>Nama Mata Pelajaran</th>
               <th>Kode Rahasia Tes</th>
               <th>Waktu Valid</th>
               <th class="action" align="center">Aksi</th>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <a href="#" title="Edit Ujian" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
                    <a href="#" title="Hapus Ujian" class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></a>
                    <a href="#" title="Manajemen Pertanyaan" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a>
                </td>
            </tr>
          </table>

                        <!-- <?php echo $pages->page_links(); ?> -->
            </div>
         </div>
      </div>
  </section>
</section>
<?php
include('footer.php'); ?>
