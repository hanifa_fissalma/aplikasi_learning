<?php
include('header.php');
?>
<section id="main-content">
  <section class="wrapper">
    <h3><i class="fa fa-mapel"></i> Daftar Mata Pelajaran</h3>
      <div class="row">
        <div class="col-md-12">
          <div class="content-panel content-table">
            <div class="action-button pull-right">
                <a href="#" class="btn btn-large btn-info button-add"><i class="glyphicon glyphicon-plus"></i> &nbsp; Tambah Mata Pelajaran</a>
            </div>
            <hr>
            <table class='table table-striped table-advance table-hover'>
            <tr>
               <th class="no">No.</th>
               <th>Nama Mata Pelajaran</th>
               <th>Deskripsi Mata Pelajaran</th>
               <th>Guru Pengampu</th>
               <th class="action" align="center">Aksi</th>
            </tr>
            <tr>
                <td></td>
                <td>  </td>
                <td></td>
                <td></td>
                <td>
                    <a href="#" title="Edit Mata pelajaran" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
                    <a href="#" title="Hapus Mata Pelajaran" class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></a>
                </td>
            </tr>
          </table>

                        <!-- <?php echo $pages->page_links(); ?> -->
            </div><! --/content-panel -->
         </div><!-- /col-md-12 -->
      </div><!-- row -->
  </section>
</section>

      <!--main content end-->

<?php
include('footer.php'); ?>
