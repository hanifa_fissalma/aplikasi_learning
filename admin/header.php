<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <title>E-LEARNING TAMAN SISWA 2 JAKARTA</title>
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="../assets/lineicons/style.css">
    <link rel="stylesheet" href="../assets/css/to-do.css">
    <link href="../assets/css/style.css" rel="stylesheet">
    <link href="assets/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="../assets/css/style-responsive.css" rel="stylesheet">
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/jquery-1.8.3.min.js"></script>
    <link rel="icon" href="../assets/img/ui-sam.png" sizes="16x16">
    <link rel="stylesheet" type="text/css" media="all" href="../calendar/jsDatePick.css" />
    <script type="text/javascript" src="../calendar/jsDatePick.full.1.1.js"></script>
  </head>
  <body>
      <section id="container" >
      <header class="header black-bg">
        <a href="dashboard.php" class="logo"><b>Sistem E-Learning SMK TAMAN SISWA 2 JAKARTA</b></a>
        <div class="top-menu">
          <ul class="nav pull-right top-menu">
            <li><a class="logout" href="logout.php">Keluar</a></li>
        </ul>
    </div>
</header>
<aside>
    <div id="sidebar"  class="nav-collapse ">
       <p class="centered"><a href="dashboard.php"><img src="../assets/img/ui-sam.png" width="100"</p></a>
       <h5 class="centered">E-Learning<br> SMK TAMAN SISWA 2 JAKARTA</h5>
       <ul class="sidebar-menu" id="nav-accordion">
         <li class="sub-menu dcjq-parent-li"><a href="dashboard.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
         <li class="sub-menu"><a href="#"><i class="fa fa-users"></i> <span>Manajemen Pengguna</span></a>
           <ul class="sub">
             <li class=""><a href="daftarguru.php"><i class="fa fa-user"></i> <span>Guru</span></a></li>
             <li class=""><a href="daftarsiswa.php"><i class="fa fa-user"></i> <span>Siswa</span></a></li>
             <li class=""><a href="daftaradmin.php"><i class="fa fa-user"></i> <span>Administrator</span></a></li>
           </ul>
         </li>
         <li class="sub-menu dcjq-parent-li"><a href="mapel.php"><i class="fa fa-book"></i> <span>Manajemen Mata Pelajaran</span></a></li>
         <li class="sub-menu dcjq-parent-li"><a href="hasilujian.php"><i class="fa fa-pencil"></i> <span>Manajemen Hasil Ujian</span></a></li>
         <li class="sub-menu dcjq-parent-li"><a href="ujian.php"><i class="fa fa-edit"></i> <span>Manajemen Ujian</span></a></li>
      </ul>
    </div>
</aside>
