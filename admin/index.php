<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <title>ADMIN - E-LEARNING TAMAN SISWA 2 JAKARTA</title>
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="../assets/css/style.css" rel="stylesheet">
    <link href="../assets/css/style-responsive.css" rel="stylesheet">
    <link rel="icon" href="../assets/img/ui-sam.png" sizes="16x16">
  </head>
  <body>
	  <div id="login-page">
	  	<div class="container">
		      <form class="form-login" action="" method="post">
		        <h2 class="form-login-heading">MASUK</h2>
		        <div class="login-wrap">
		            <input type="text" class="form-control" name="username" placeholder="ID Pengguna / NIK" autofocus>
		            <br>
		            <input type="password" class="form-control" name="password" placeholder="Password">
                <br>
		            <button type="submit" class="btn btn-theme btn-block" name="btn-login"><i class="fa fa-lock"></i> MASUK</button>
		        </div>
		      </form>
	  	</div>
	  </div>
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("assets/img/sinau.jpg", {speed: 500});
    </script>
  </body>
</html>
