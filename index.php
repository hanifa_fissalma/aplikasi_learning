<?php
session_start();
require_once 'db.php';
if (isset($_POST['submit'])) {
    if (isset($_POST['username']) && isset($_POST['password'])) {
        $username = $_POST['username'];
        $password = MD5($_POST['password']);
        $query = "SELECT * FROM `student` WHERE stdusername='$username'";
        $stmnt = $dbh->prepare($query);
        $stmnt->execute();
        if ($stmnt->rowCount() == 0) {
            echo '<script type="text/javascript">alert("Username tidak ditemukan");window.location.replace("index.php");</script>';
        } else {
            $query = "SELECT * FROM `student` WHERE stdusername='$username' and stdpassword='$password'";
            $stmnt = $dbh->prepare($query);
            $stmnt->execute();
            if ($stmnt->rowCount() == 0) {
                echo "<script type='text/javascript'>alert('Password anda salah'); window.location.replace('index.php');</script>";
            } else {
                while ($row = $stmnt->fetch()) {
                    $_SESSION['stdusername'] = $row['stdusername'];
                    echo '<script>alert("Berhasil Masuk");window.location.replace("dashboard.php");</script>';
                }
            }
        }
    } else {
        header("Location: index.php");
    }
}
?>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <title>E-LEARNING TAMAN SISWA 2 JAKARTA</title
        <!-- Bootstrap core CSS -->
        <link href="assets/css/bootstrap.css" rel="stylesheet">
        <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href="assets/css/style.css" rel="stylesheet">
        <link href="assets/css/style-responsive.css" rel="stylesheet">
        <link rel="icon" href="assets/img/ui-sam.png" sizes="16x16">
    </head>
    <body onload="getTime()">
        <div class="container">
            <div class="text-center">
                <div id="showtime"></div>
            </div>
            <div class="col-lg-4 col-lg-offset-4">
                <div class="lock-screen">
                    <h2><a data-toggle="modal" href="#myModal"><i class="fa fa-lock"></i></a></h2>
                    <p style="color:#fff">LOGIN UNTUK MEMULAI</p>
                    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Silahkan Login</h4>
                                </div>
                                <form class="form-login-modal" id="stdloginform" action="index.php" method="post">
                                    <div class="modal-body">
                                        <input type="text" class="form-control" name="username" placeholder="Nomor Induk Siswa" autofocus>
                                        <br>
                                        <input type="password" class="form-control" name="password" placeholder="Kata Sandi">
                                    </div>
                                    <div class="modal-footer centered">
                                        <button class="btn btn-theme03" form="stdloginform"  name="submit"  type="submit" name="btn-login">Login</button>
                                        <button data-dismiss="modal"  class="btn btn-theme04" type="button">Kembali</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="assets/js/jquery.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/js/jquery.backstretch.min.js"></script>
        <script>
            $.backstretch("assets/img/aa.jpg", {speed: 500});
        </script>
        <script>
            function getTime()
            {
                var today=new Date();
                var h=today.getHours();
                var m=today.getMinutes();
                var s=today.getSeconds();
                // add a zero in front of numbers<10
                m=checkTime(m);
                s=checkTime(s);
                document.getElementById('showtime').innerHTML=h+":"+m+":"+s;
                t=setTimeout(function(){getTime()},500);
            }

            function checkTime(i)
            {
                if (i<10)
                {
                    i="0" + i;
                }
                return i;
            }
        </script>
    </body>
</html>
