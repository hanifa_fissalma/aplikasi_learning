<?php
	include_once "db.php"; 
	include_once "ceklogin.php"; 
	include_once "header.php";
	$id=$_GET['id'];
    $username= $_SESSION['stdusername'];
    
    $query = "SELECT test.testid, test.testname, test.testdesc, subject.subname, test.duration, test.totalquestions, class.classname
                FROM subject, test, class
                WHERE test.subid = subject.subid AND test.classid=class.classid AND test.testid=$id";
    $stmnt = $dbh->prepare($query);
    $stmnt->execute();
    $data = $stmnt->fetch();
    
    if (isset($_POST['cek'])) {
        
    }
?>
<section id="main-content">
<section class="wrapper">
  <h3><i class="fa fa-mapel"></i>MASUKKAN KODE TES</h3>
  <div class="row">
    <div class="col-md-12">
      <div class="content-panel content-table">
        <form class="form-add" method='post'>
              <table class='table table-bordered'>
                  <tr>
                        <td>Nama Ujian</td>
                        <td><?php echo $data['testname'] ?></td>
                  </tr>
                  <tr>
                        <td>Nama Mata Pelajaran</td>
                        <td><?php echo $data['subname'] ?></td>
                  </tr>
                  <tr>
                        <td>Kelas</td>
                        <td><?php echo $data['classname'] ?></td>
                  </tr>
                  <tr>
                        <td>Deskripsi Tes</td>
                        <td><?php echo $data['testdesc'] ?></td>
                  </tr>
                  <tr>
                        <td>Durasi</td>
                        <td><?php echo $data['duration'] ?> Menit</td>
                  </tr>
                  <tr>
                        <td>Total Pertanyaan</td>
                        <td><?php echo $data['totalquestions'] ?> Pertanyaan</td>
                  </tr>
                  <tr>
                      <td>Kode Tes</td>
                      <td><input type='text' name='testcode' class='form-control'></td>
                  </tr>
                  <tr>
                      <td colspan="2">
                          <button type="submit" class="btn btn-primary" name="cek" onClick="cek()">
                              <span class="glyphicon glyphicon-edit"></span>  Mulai Tes
                          </button>
                      </td>
                  </tr>

              </table>
          </form>

      </div>
    </div>
  </div>
</section>
</section>
<?php
include('footer.php'); ?>
