-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 12, 2018 at 01:57 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aplikasi`
--

-- --------------------------------------------------------

--
-- Table structure for table `adminlogin`
--

CREATE TABLE `adminlogin` (
  `admid` int(11) NOT NULL,
  `admusername` varchar(32) NOT NULL,
  `admfullname` varchar(100) NOT NULL,
  `admpassword` varchar(32) NOT NULL,
  `admemail` varchar(50) NOT NULL,
  `admcontact` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE `class` (
  `classid` int(11) NOT NULL,
  `classname` varchar(20) NOT NULL,
  `totalstudent` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`classid`, `classname`, `totalstudent`) VALUES
(1, 'X TKJ', 30),
(2, 'X MM', 30);

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `testid` int(11) NOT NULL,
  `qnid` int(11) NOT NULL,
  `question` varchar(500) NOT NULL,
  `optiona` varchar(100) NOT NULL,
  `optionb` varchar(100) NOT NULL,
  `optionc` varchar(100) NOT NULL,
  `optiond` varchar(100) NOT NULL,
  `correctanswer` enum('optiona','optionb','optionc','optiond') NOT NULL,
  `marks` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`testid`, `qnid`, `question`, `optiona`, `optionb`, `optionc`, `optiond`, `correctanswer`, `marks`) VALUES
(1, 2, '10-2', '8', '6', '5', '4', 'optiond', 10),
(1, 3, '90-10', '80', '78', '56', '43', 'optiona', 10),
(2, 8, 'apa itu?', 'buah', 'sayur', 'benda', 'manusia', 'optionc', 10),
(1, 9, 'apa itu?', 'manusia', 'hewan', 'tumbuhan', 'benda', 'optionc', 10);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `stdid` int(11) NOT NULL,
  `stdusername` varchar(40) NOT NULL,
  `stdfullname` varchar(100) NOT NULL,
  `stdpassword` varchar(40) NOT NULL,
  `classid` int(11) NOT NULL,
  `emailid` varchar(40) NOT NULL,
  `contactno` varchar(20) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`stdid`, `stdusername`, `stdfullname`, `stdpassword`, `classid`, `emailid`, `contactno`, `address`) VALUES
(1, '123456', 'Hanifa Fissalma', '76e105c3a61db1b3f13207774aeccc3c', 2, 'halimiliul@gmail.co.id', '087886735414', 'Jl. jalan jalan');

-- --------------------------------------------------------

--
-- Table structure for table `studentquestion`
--

CREATE TABLE `studentquestion` (
  `stdqn` int(11) NOT NULL,
  `stdid` int(11) NOT NULL,
  `testid` int(11) NOT NULL,
  `qnid` int(11) NOT NULL,
  `answered` enum('answered','unanswered','review','') NOT NULL,
  `stdanswer` enum('optiona','optionb','optionc','optiond') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `studenttest`
--

CREATE TABLE `studenttest` (
  `stdtest` int(11) NOT NULL,
  `stdid` int(11) NOT NULL,
  `testid` int(11) NOT NULL,
  `starttime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `endtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `correctlyanswered` int(11) NOT NULL,
  `status` enum('over','inprogress','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE `subject` (
  `subid` int(11) NOT NULL,
  `classid` int(11) NOT NULL,
  `subname` varchar(40) NOT NULL,
  `subdesc` varchar(100) NOT NULL,
  `tcid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`subid`, `classid`, `subname`, `subdesc`, `tcid`) VALUES
(1, 2, 'Matematika', 'ini', 1),
(2, 2, 'Bahasa Inggris', 'fegrhrhefwfwf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `testid` int(11) NOT NULL,
  `testname` varchar(30) NOT NULL,
  `classid` int(11) NOT NULL,
  `testdesc` varchar(100) DEFAULT NULL,
  `testdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `subid` int(11) NOT NULL,
  `testfrom` date NOT NULL,
  `testto` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `duration` int(11) NOT NULL,
  `totalquestions` int(11) NOT NULL,
  `attemptedstudents` bigint(20) NOT NULL,
  `testcode` varchar(40) NOT NULL,
  `tcid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`testid`, `testname`, `classid`, `testdesc`, `testdate`, `subid`, `testfrom`, `testto`, `duration`, `totalquestions`, `attemptedstudents`, `testcode`, `tcid`) VALUES
(1, 'UTS Matematika', 2, 'ujian ini berlangsung selama 2 jam bla bla bla', '2018-03-01 06:14:33', 1, '2018-03-01', '2018-03-02 17:00:00', 10, 10, 0, '1234', 1),
(2, 'UAS', 1, 'fenjfekfmekmfkefkemk', '2018-03-07 02:55:54', 1, '2018-03-07', '2018-03-15 17:00:00', 10, 10, 0, '12345', 1);

-- --------------------------------------------------------

--
-- Table structure for table `testconductor`
--

CREATE TABLE `testconductor` (
  `tcid` int(11) NOT NULL,
  `tcusername` varchar(40) NOT NULL,
  `tcfullname` varchar(100) NOT NULL,
  `tcpassword` varchar(40) NOT NULL,
  `emailid` varchar(40) NOT NULL,
  `contactno` varchar(20) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testconductor`
--

INSERT INTO `testconductor` (`tcid`, `tcusername`, `tcfullname`, `tcpassword`, `emailid`, `contactno`, `address`) VALUES
(1, '123456789', 'Hanifa Fissalma', '76e105c3a61db1b3f13207774aeccc3c', 'halimiliul@gmail.co.id', '087886735414', 'Jl, Rawamangun Muka');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adminlogin`
--
ALTER TABLE `adminlogin`
  ADD PRIMARY KEY (`admid`),
  ADD UNIQUE KEY `admusername` (`admusername`);

--
-- Indexes for table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`classid`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`qnid`),
  ADD KEY `testid` (`testid`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`stdid`);

--
-- Indexes for table `studentquestion`
--
ALTER TABLE `studentquestion`
  ADD PRIMARY KEY (`stdqn`),
  ADD KEY `stdid` (`stdid`),
  ADD KEY `qnid` (`qnid`),
  ADD KEY `testid` (`testid`);

--
-- Indexes for table `studenttest`
--
ALTER TABLE `studenttest`
  ADD PRIMARY KEY (`stdtest`),
  ADD KEY `stdid` (`stdid`),
  ADD KEY `testid` (`testid`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`subid`),
  ADD KEY `classid` (`classid`),
  ADD KEY `tcid` (`tcid`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`testid`),
  ADD KEY `classid` (`classid`),
  ADD KEY `subid` (`subid`),
  ADD KEY `tcid` (`tcid`);

--
-- Indexes for table `testconductor`
--
ALTER TABLE `testconductor`
  ADD PRIMARY KEY (`tcid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adminlogin`
--
ALTER TABLE `adminlogin`
  MODIFY `admid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `class`
--
ALTER TABLE `class`
  MODIFY `classid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `qnid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `stdid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `studentquestion`
--
ALTER TABLE `studentquestion`
  MODIFY `stdqn` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `studenttest`
--
ALTER TABLE `studenttest`
  MODIFY `stdtest` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subject`
--
ALTER TABLE `subject`
  MODIFY `subid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `testid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `testconductor`
--
ALTER TABLE `testconductor`
  MODIFY `tcid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `question_ibfk_1` FOREIGN KEY (`testid`) REFERENCES `test` (`testid`);

--
-- Constraints for table `studentquestion`
--
ALTER TABLE `studentquestion`
  ADD CONSTRAINT `studentquestion_ibfk_1` FOREIGN KEY (`stdid`) REFERENCES `student` (`stdid`),
  ADD CONSTRAINT `studentquestion_ibfk_2` FOREIGN KEY (`qnid`) REFERENCES `question` (`qnid`),
  ADD CONSTRAINT `studentquestion_ibfk_3` FOREIGN KEY (`testid`) REFERENCES `test` (`testid`);

--
-- Constraints for table `studenttest`
--
ALTER TABLE `studenttest`
  ADD CONSTRAINT `studenttest_ibfk_1` FOREIGN KEY (`stdid`) REFERENCES `student` (`stdid`),
  ADD CONSTRAINT `studenttest_ibfk_2` FOREIGN KEY (`testid`) REFERENCES `test` (`testid`);

--
-- Constraints for table `subject`
--
ALTER TABLE `subject`
  ADD CONSTRAINT `subject_ibfk_1` FOREIGN KEY (`classid`) REFERENCES `class` (`classid`),
  ADD CONSTRAINT `subject_ibfk_2` FOREIGN KEY (`tcid`) REFERENCES `testconductor` (`tcid`);

--
-- Constraints for table `test`
--
ALTER TABLE `test`
  ADD CONSTRAINT `test_ibfk_1` FOREIGN KEY (`classid`) REFERENCES `class` (`classid`),
  ADD CONSTRAINT `test_ibfk_2` FOREIGN KEY (`subid`) REFERENCES `subject` (`subid`),
  ADD CONSTRAINT `test_ibfk_3` FOREIGN KEY (`tcid`) REFERENCES `testconductor` (`tcid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
