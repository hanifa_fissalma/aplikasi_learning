<?php
  include '../db.php';
  include 'ceklogin.php';
 ?>
<?php
  include('header.php');
  $query = "SELECT * FROM `testconductor` WHERE tcusername=".$_SESSION['tcusername'];
  $stmnt = $dbh->prepare($query);
  $stmnt->execute();
  $data = $stmnt->fetch();
?>

<section id="main-content">
<section class="wrapper">
  <h3><i class="fa fa-mapel"></i> UBAH PROFIL </h3>
  <div class="row">
    <div class="col-md-12">
      <div class="content-panel content-table">
        <form class="form-add" method='post'action="editprofilf.php">
              <table class='table table-bordered'>
                  <tr>
                      <td>Nomor Induk Pegawai</td>
                      <td><input type='text' name='tcname' class='form-control' value="<?php echo $_SESSION['tcusername'] ?>" readonly></td>
                  </tr>
                  <tr>
                      <td>Nama Lengkap</td>
                      <td><input type='text' name='tcfullname' class='form-control' value="<?php echo $data['tcfullname'] ?>" required></td>
                  </tr>
                  <tr>
                      <td>Password</td>
                      <td><input type='password' name='tcpassword' class='form-control' value="<?php echo $data['tcpassword'] ?>" readonly></td>
                  </tr>
                  <tr>
                      <td>Email</td>
                      <td><input type='text' name='emailid' class='form-control' value="<?php echo $data['emailid'] ?>" required></td>
                  </tr>
                  <tr>
                      <td>Nomor Telepon/HP</td>
                      <td><input type='text' name='contactno' class='form-control' value="<?php echo $data['contactno'] ?>" required></td>
                  </tr>
                  <tr>
                      <td>Alamat</td>
                      <td><input type='text' name='address' class='form-control' value="<?php echo $data['address'] ?>" required></td>
                  </tr>
                  <tr>
                      <td colspan="2">
                          <button type="submit" class="btn btn-primary" name="btn-update">
                              <span class="glyphicon glyphicon-edit"></span>  Update
                          </button>
                          <a href="#" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Batal</a>
                      </td>
                  </tr>

              </table>
          </form>

      </div>
    </div>
  </div>
</section>
</section>
<?php
include('footer.php'); ?>
