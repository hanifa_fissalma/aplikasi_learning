<?php
include('header.php');
?>
<section id="main-content">
  <section class="wrapper">
    <h3><i class="fa fa-mapel"></i> Daftar Mata Pelajaran</h3>
      <div class="row">
        <div class="col-md-12">
          <div class="content-panel content-table">
            <div class="action-button pull-right">
                <a href="tambahmapel.php" class="btn btn-large btn-info button-add"><i class="glyphicon glyphicon-plus"></i> &nbsp; Tambah Mata Pelajaran</a>
            </div>
            <hr>
            <?php
              require_once '../db.php';
              $sql="SELECT subject.subid, subject.subname, subject.subdesc, class.classname, testconductor.tcfullname
                    FROM subject, class, testconductor
                    WHERE subject.classid = class.classid
                    AND subject.tcid = testconductor.tcid
                    order by subid";
              $stmnt = $dbh->prepare($sql);
              $stmnt->execute();
              if ($stmnt->rowCount() == 0) {
                  echo'Tidak Ada Data';
              } else {
                echo
                  "<table id='mapel' class='table table-striped table-bordered' cellspacing='0' width='100%'>
                    <thead>
                      <tr>
                         <th>Kode</th>
                         <th>Nama Mata Pelajaran</th>
                         <th>Kelas</th>
                         <th>Deskripsi Mata Pelajaran</th>
                         <th>Guru Pengampu</th>
                         <th class='action' align='center'>Aksi</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                         <th>Kode</th>
                         <th>Nama Mata Pelajaran</th>
                         <th>Kelas</th>
                         <th>Deskripsi Mata Pelajaran</th>
                         <th>Guru Pengampu</th>
                         <th class='action' align='center'>Aksi</th>
                      </tr>
                    </tfoot>
                    <tbody>";
                        while ($row = $stmnt->fetch()){
    												$subid=$row['subid'];
                            $classid=$row['classname'];
    												$subname=$row['subname'];
    												$subdesc=$row['subdesc'];
    												$tcid=$row['tcfullname'];
    												echo
                          "<tr>
                              <td>$subid</td>
                              <td>$subname</td>
                              <td>$classid</td>
                              <td>$subdesc</td>
                              <td>$tcid</td>
                              <td>
                                  <a href='editmapel.php?id=" . $row['subid'] . "' class='btn btn-xs btn-warning' title='Ubah'><i class='fa fa-edit'></i></a>
                                  <a href='deletemapel.php?id=" . $row['subid'] . "' class='btn btn-xs btn-danger' title='Hapus'><i class='fa fa-trash-o'></i></a>
                              </td>
                          </tr>";
                        }
                        echo
                    "</tbody>
                 </table>";
               }
               ?>
            </div><! --/content-panel -->
         </div><!-- /col-md-12 -->
      </div><!-- row -->
  </section>
</section>

<script>
  $(document).ready(function(){
    $("#mapel").DataTable();
  });
</script>
<?php
  include('footer.php'); 
?>
