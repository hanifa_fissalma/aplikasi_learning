<?php
  include('header.php');
?>
<?php
    include 'ceklogin.php';
    $testid = filter_input(INPUT_POST, "testid");
    $testname = filter_input(INPUT_POST, "testname");
    $classid = filter_input(INPUT_POST, "classid");
    $testdesc = filter_input(INPUT_POST, "testdesc");
    $subid = filter_input(INPUT_POST, "subid");
    $testfrom = filter_input(INPUT_POST,"testfrom");
    $testto = filter_input(INPUT_POST,"testto");
    $totalqn = filter_input(INPUT_POST,"totalqn");
    $duration = filter_input(INPUT_POST, "duration");
    $testcode = filter_input(INPUT_POST,"testcode");
    $tcid = filter_input(INPUT_POST, "tcid");
    require_once '../db.php';
    $id=$_GET['id'];
    $stmnt = $dbh->prepare("SELECT * from test WHERE testid = '$id'");
    $stmnt->execute();
    $data = $stmnt->fetch();
?>
<section id="main-content">
<section class="wrapper">
  <h3><i class="fa fa-mapel"></i> TAMBAH UJIAN </h3>
  <div class="row">
    <div class="col-md-12">
      <div class="content-panel content-table">
        <form class="form-add" method='post'action="editujianf.php">
              <table class='table table-bordered'>
                  <tr>
                      <td>Kode Ujian</td>
                      <td><input type='number' name='testid' class='form-control' value="<?php echo $data['testid'] ?>" readonly></td>
                  </tr>
                   <tr>
                      <td>Mata Pelajaran</td>
                      <td>
                        <select id="level" class="form-control" name="subid" value="<?php echo $data['subid'] ?>" required>
                            <option value="">Silahkan Pilih Mata Pelajaran</option>
                            <?php
                              require_once '../db.php';
                              $sql="SELECT * FROM `subject` ";
                              $stmnt = $dbh->prepare($sql);
                              $stmnt->execute();
                              while ($subject = $stmnt->fetch()){
                                $value="";
                                $subid=$subject['subid'];
                                $subname=$subject['subname'];
                                if($subid == $data['subid']){
                                    $value="selected";
                                }
                                echo "<option value='$subid' ".$value.">$subname</option>";
                              }
                            ?>
                        </select>
                      </td>
                  </tr>
                  <tr>
                      <td>Kelas</td>
                      <td>
                        <select id="level" class="form-control" name="classid" value="<?php echo $data['classid'] ?>" required>
                            <option value="">Silahkan Pilih Kelas</option>
                            <?php
                              require_once '../db.php';
                              $sql="SELECT * FROM `class` ";
                              $stmnt = $dbh->prepare($sql);
                              $stmnt->execute();
                              while ($class = $stmnt->fetch()){
                                $value="";
                                $classid=$class['classid'];
                                $classname=$class['classname'];
                                if($classid == $data['classid']){
                                    $value="selected";
                                }
                                echo "<option value='$classid' ".$value.">$classname</option>";
                              }
                            ?>
                        </select>
                      </td>
                  </tr>
                  <tr>
                      <td>Nama Ujian</td>
                      <td><input type='text' name='testname' class='form-control' value="<?php echo $data['testname'] ?>" required></td>
                  </tr>
                  <tr>
                      <td>Deskripsi Ujian</td>
                      <td><textarea type='text' name='testdesc' class='form-control'  required><?php echo $data['testdesc'] ?></textarea></td>
                  </tr>
                  <tr>
                      <td>Total Pertanyaan</td>
                      <td><input type='number' name='totalqn' class='form-control' value="<?php echo $data['totalquestions'] ?>" required></td>
                  </tr>
                  <tr>
                      <td>Durasi (dalam menit)</td>
                      <td><input type='number' name='duration' class='form-control' value="<?php echo $data['duration'] ?>" required></td>
                  </tr>
                  <tr>
                      <td>Waktu Tes Dari</td>
                      <td><input type='date' id="testfrom" name='testfrom' class='form-control' value="<?php echo $data['testfrom'] ?>" required></td>
                  </tr>
                  <tr>
                      <td>Waktu Akhir Tes</td>
                      <td><input type='date' id="testto" name='testto' class='form-control' value="<?php echo $data['testto'] ?>" required></td>
                  </tr>
                  <tr>
                      <td>Kode Tes</td>
                      <td><input type='number' name='testcode' class='form-control' value="<?php echo $data['testcode'] ?>" required></td>
                  </tr>
                  <tr>
                      <td>Guru Pengampu</td>
                      <td>
                        <select id="level" class="form-control" name="tcid" value="<?php echo $data['tcid'] ?>" required>
                            <option value="">Silahkan Pilih Guru Pengampu</option>
                            <?php
                              require_once '../db.php';
                              $sql="SELECT * FROM `testconductor` ";
                              $stmnt = $dbh->prepare($sql);
                              $stmnt->execute();
                              while ($teacher = $stmnt->fetch()){
                                $value="";
                                $tcid=$teacher['tcid'];
                                $tcname=$teacher['tcfullname'];
                                if($tcid == $data['tcid']){
                                    $value="selected";
                                }
                                echo "<option value='$tcid' ".$value.">$tcname</option>";
                              }
                            ?>
                        </select>
                      </td>
                  </tr>
                  <tr>
                      <td colspan="2">
                          <button type="submit" class="btn btn-primary" name="btn-update">
                              <span class="glyphicon glyphicon-edit"></span>  Update
                          </button>
                          <a href="ujian.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Batal</a>
                      </td>
                  </tr>

              </table>
          </form>

      </div>
    </div>
  </div>
</section>
</section>
<?php
include('footer.php'); ?>
