<?php
include('header.php');
?>
<section id="main-content">
  <section class="wrapper">
    <h3><i class="fa fa-mapel"></i> Daftar Hasil Ujian</h3>
      <div class="row">
        <div class="col-md-12">
          <div class="content-panel content-table">
            <?php
              require_once '../db.php';
              $sql="SELECT test.testid, test.testname, test.testfrom, test.testto, subject.subname, test.attemptedstudents
                    FROM subject, test
                    WHERE test.subid = subject.subid
                    order by testid";
              $stmnt = $dbh->prepare($sql);
              $stmnt->execute();
              if ($stmnt->rowCount() == 0) {
                  echo'Tidak Ada Data';
              } else {
                echo
                  "<table id='hasilujian' class='table table-striped table-bordered' cellspacing='0' width='100%'> 
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Ujian</th>
                        <th>Waktu Valid</th>
                        <th>Nama Mata Pelajaran</th>
                        <th>Siswa yang Mengikuti</th>
                        <th class='action' align='center'>Aksi</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <th>No</th>
                        <th>Nama Ujian</th>
                        <th>Waktu Valid</th>
                        <th>Nama Mata Pelajaran</th>
                        <th>Siswa yang Mengikuti</th>
                        <th class='action' align='center'>Aksi</th>
                      </tr>
                    </tfoot>";
                    while ($row = $stmnt->fetch()){
                      $testid=$row['testid'];
                      $testname=$row['testname'];
                      $testfrom=$row['testfrom'];
                      $testto=$row['testto'];
                      $subname=$row['subname'];
                      $attemptedstudents=$row['attemptedstudents'];
                      echo
                    "<tbody>
                      <tr>
                        <td>$testid</td>
                        <td>$testname</td>
                        <td>$testfrom s.d. $testto</td>
                        <td>$subname</td>
                        <td>$attemptedstudents</td>
                        <td>
                            <a href='detailujian.php?id=" . $row['testid'] . "' title='Detail Hasil Ujian' class='btn btn-primary btn-xs'><i class='fa fa-eye'></i></a>
                        </td>
                      </tr>";
                    }
                    echo
                    "</tbody>
                  </table>";
                  }
                  ?>
          </div>
        </div>
      </div>
  </section>
</section>
<script>
  $(document).ready(function(){
    $("#hasilujian").DataTable();
  });
</script>
<?php
include('footer.php'); ?>
