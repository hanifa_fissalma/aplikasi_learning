<?php
  include 'ceklogin.php';
  include('header.php');
    $testid = filter_input(INPUT_POST, "testid");
    $qnid = filter_input(INPUT_POST, "qnid");
    $question = filter_input(INPUT_POST, "question");
    $optiona = filter_input(INPUT_POST, "optiona");
    $optionb = filter_input(INPUT_POST, "optionb");
    $optionc = filter_input(INPUT_POST, "optionc");
    $optiond = filter_input(INPUT_POST, "optiond");
    $correctanswer = filter_input(INPUT_POST, "correctanswer");
    $marks = filter_input(INPUT_POST, "marks");
    require_once '../db.php';
    $id=$_GET['id'];
    $stmnt = $dbh->prepare("SELECT * from question WHERE qnid = '$id'");
    $stmnt->execute();
    $data = $stmnt->fetch();
?>
<section id="main-content">
  <section class="wrapper">
    <h3><i class="fa fa-mapel"></i> Ubah Pertanyaan</h3>
      <div class="row">
        <div class="col-md-12">
          <div class="content-panel content-table">
            <form class="form-add" method="post" action="editpertanyaanf.php">
                <table class='table table-bordered'>
                    <tr>
                        <td>Kode Tes</td>
                        <td><input type='text' name='testid' class='form-control' value="<?php echo $data['testid'] ?>" readonly></td>
                    </tr>
                    <tr>
                        <td>Kode Pertanyaan</td>
                        <td><input type='text' name='qnid' class='form-control' value="<?php echo $data['qnid'] ?>" readonly></td>
                    </tr>
                    <tr>
                        <td>Pertanyaan</td>
                        <td><textarea type='text' name='question' class='form-control'  required><?php echo $data['question'] ?></textarea></td>
                    </tr>
                    
                    <tr>
                        <td>Pilihan A</td>
                        <td><input type='text' name='optiona' class='form-control' value="<?php echo $data['optiona'] ?>"  required></td>
                    </tr>
                    <tr>
                        <td>Pilihan B</td>
                        <td><input type='text' name='optionb' class='form-control' value="<?php echo $data['optionb'] ?>"  required></td>
                    </tr>
                    <tr>
                        <td>Pilihan C</td>
                        <td><input type='text' name='optionc' class='form-control' value="<?php echo $data['optionc'] ?>"  required></td>
                    </tr>
                    <tr>
                        <td>Pilihan D</td>
                        <td><input type='text' name='optiond' class='form-control' value="<?php echo $data['optiond'] ?>"  required></td>
                    </tr>
                    <tr>
                        <td>Jawaban Benar</td>
                        <td>
                          <select id="level" class="form-control" name="correctanswer" required>
                              <option value="">Silahkan Pilih Jawaban Benar</option>
                              <option  <?php if ($data['correctanswer'] == "optiona" ) echo 'selected' ; ?> value="optiona">Pilihan A</option>
                              <option  <?php if ($data['correctanswer'] == "optionb" ) echo 'selected' ; ?> value="optionb">Pilihan B</option>
                              <option  <?php if ($data['correctanswer'] == "optionc" ) echo 'selected' ; ?> value="optionc">Pilihan C</option>
                              <option  <?php if ($data['correctanswer'] == "optiond" ) echo 'selected' ; ?> value="optiond">Pilihan D</option>
                          </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Bobot Nilai</td>
                        <td><input type='text' name='marks' class='form-control' value="<?php echo $data['marks'] ?>" required></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button type="submit" class="btn btn-primary" name="btn-update">
                                <span class="glyphicon glyphicon-edit"></span>  Update
                            </button>
                            <a href="ujian.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Batal</a>
                        </td>
                    </tr>
                </table>
            </form>
          </div>
        </div>
      </div>
  </section>
</section>

<?php
include('footer.php'); ?>
