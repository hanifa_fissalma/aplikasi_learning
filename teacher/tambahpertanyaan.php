<?php
  include('header.php');
    $id=$_GET['id'];
    if (isset($_POST['btn-add'])) {
      $testid = filter_input(INPUT_POST, "testid");
      $question = filter_input(INPUT_POST, "question");
      $optiona = filter_input(INPUT_POST, "optiona");
      $optionb = filter_input(INPUT_POST, "optionb");
      $optionc = filter_input(INPUT_POST, "optionc");
      $optiond = filter_input(INPUT_POST, "optiond");
      $correctanswer = filter_input(INPUT_POST, "correctanswer");
      $marks = filter_input(INPUT_POST, "marks");
        require_once '../db.php';
        // $stmnt = $dbh->prepare("SELECT * FROM `question` where qnid = '" . $qnid . "'");
        // $stmnt->execute();
        // $row = $stmnt->rowCount();
        // if ($row == 0) {
            $query = "INSERT INTO `question` (testid, question, optiona, optionb, optionc, optiond, correctanswer, marks)
               VALUES('" . $testid . "','" . $question . "','".$optiona."', '".$optionb."',
                      '".$optionc."','".$optiond."','".$correctanswer."','".$marks."')";
            $stmnt2 = $dbh->prepare($query);
            $stmnt2->execute();
            if ($stmnt2) {
                echo "<script>alert('Berhasil Menambah Pertanyaan');window.location.replace('pertanyaan.php?id=".$id."');</script>";
            } else {
                echo "<script>alert('Gagal Menambahkan Pertanyaan')</script>";
            }
        // } else {
        //     echo '<script>alert("Pertanyaan telah ada")</script>';
        // }
    }
?>
<section id="main-content">
  <section class="wrapper">
    <h3><i class="fa fa-mapel"></i> Tambah Pertanyaan</h3>
      <div class="row">
        <div class="col-md-12">
          <div class="content-panel content-table">
            <form class="form-add" method='post'action="tambahpertanyaan.php?id=<?php echo $id ?>">
                <table class='table table-bordered'>
                    <tr>
                        <td>Kode Tes</td>
                        <td><input type='text' name='testid' class='form-control' value="<?php echo $id ?>" readonly></td>
                    </tr>
                    <tr>
                        <td>Pertanyaan</td>
                        <td><textarea type='text' name='question' class='form-control' required></textarea></td>
                    </tr>
                    
                    <tr>
                        <td>Pilihan A</td>
                        <td><input type='text' name='optiona' class='form-control' required></td>
                    </tr>
                    <tr>
                        <td>Pilihan B</td>
                        <td><input type='text' name='optionb' class='form-control' required></td>
                    </tr>
                    <tr>
                        <td>Pilihan C</td>
                        <td><input type='text' name='optionc' class='form-control' required></td>
                    </tr>
                    <tr>
                        <td>Pilihan D</td>
                        <td><input type='text' name='optiond' class='form-control' required></td>
                    </tr>
                    <tr>
                        <td>Jawaban Benar</td>
                        <td>
                          <select id="level" class="form-control" name="correctanswer" required>
                              <option value="">Silahkan Pilih Jawaban Benar</option>
                              <option value="optiona">Pilihan A</option>
                              <option value="optionb">Pilihan B</option>
                              <option value="optionc">Pilihan C</option>
                              <option value="optiond">Pilihan D</option>
                          </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Bobot Nilai</td>
                        <td><input type='text' name='marks' class='form-control' required></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button type="submit" class="btn btn-primary" name="btn-add">
                                <span class="glyphicon glyphicon-edit"></span>  Tambah
                            </button>
                            <a href="#" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Batal</a>
                        </td>
                    </tr>
                </table>
            </form>
          </div>
        </div>
      </div>
  </section>
</section>

<?php
include('footer.php'); ?>
