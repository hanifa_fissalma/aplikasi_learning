<?php
include('header.php');
?>
<section id="main-content">
  <section class="wrapper">
    <h3><i class="fa fa-mapel"></i> Daftar Ujian</h3>
      <div class="row">
        <div class="col-md-12">
          <div class="content-panel content-table">
            <div class="action-button pull-right">
                <a href="tambahujian.php" class="btn btn-large btn-info button-add"><i class="glyphicon glyphicon-plus"></i> &nbsp; Tambah Ujian</a>
            </div>
            <hr>
            <?php
              require_once '../db.php';
              $sql="SELECT test.testid, test.testname, test.testdesc, subject.subname, class.classname, test.testcode, test.testfrom, test.testto
                    FROM subject, class, test
                    WHERE test.classid = class.classid
                    AND test.subid = subject.subid
                    order by testid";
              $stmnt = $dbh->prepare($sql);
              $stmnt->execute();
              if ($stmnt->rowCount() == 0) {
                  echo'Tidak Ada Data';
              } else {
                echo
            "<table id='ujian' class='table table-striped table-bordered' cellspacing='0' width='100%'>
              <thead>
                <tr>
                  <th>Kode</th>
                  <th>Nama Ujian</th>
                  <th>Deskripsi Ujian</th>
                  <th>Nama Mata Pelajaran</th>
                  <th>Kelas</th>
                  <th>Kode Rahasia Tes</th>
                  <th>Waktu Valid</th>
                  <th class='action' align='center'>Aksi</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Kode</th>
                  <th>Nama Ujian</th>
                  <th>Deskripsi Ujian</th>
                  <th>Nama Mata Pelajaran</th>
                  <th>Kelas</th>
                  <th>Kode Rahasia Tes</th>
                  <th>Waktu Valid</th>
                  <th class='action' align='center'>Aksi</th>
                </tr>
              </tfoot>
              <tbody>";
              while ($row = $stmnt->fetch()){
                $testid=$row['testid'];
                $testname=$row['testname'];
                $testdesc=$row['testdesc'];
                $subname=$row['subname'];
                $classname=$row['classname'];
                $testcode=$row['testcode'];
                $testfrom=$row['testfrom'];
                $testto=$row['testto'];
                echo
                "<tr>
                  <td>$testid</td>
                  <td>$testname</td>
                  <td>$testdesc</td>
                  <td>$subname</td>
                  <td>$classname</td>
                  <td>$testcode</td>
                  <td>$testfrom s.d. $testto</td>
                  <td>
                    <a href='editujian.php?id=" . $row['testid'] . "' class='btn btn-xs btn-warning' title='Ubah'><i class='fa fa-edit'></i></a>
                    <a href='deleteujian.php?id=" . $row['testid'] . "' class='btn btn-xs btn-danger' title='Hapus'><i class='fa fa-trash-o'></i></a>
                    <a href='pertanyaan.php?id=" . $row['testid'] . "' class='btn btn-xs btn-success' title='Manajemen Pertanyaan'><i class='fa fa-book'></i></a>
                  </td>
                </tr>";
              }
              "</tbody>
          </table>";
            }
            ?>
            </div>
         </div>
      </div>
  </section>
</section>
<script>
  $(document).ready(function() {
    $('#ujian').DataTable();
} );
</script>
<?php
include('footer.php'); ?>
