<?php
  include('header.php');
?>
<?php
  include 'ceklogin.php';
    $testname = filter_input(INPUT_POST, "testname");
    $testfrom = filter_input(INPUT_POST, "testfrom");
    $testto = filter_input(INPUT_POST, "testto");
    $subname = filter_input(INPUT_POST,"subname");
    $attemptedstudents = filter_input(INPUT_POST, "attemptedstudents");
    require_once '../db.php';
    $id=$_GET['id'];
    $stmnt = $dbh->prepare("SELECT test.testname, test.testfrom, test.testto, subject.subname, test.attemptedstudents
                            FROM subject, test
                            WHERE test.subid = subject.subid AND testid = $id"
                          );
    $stmnt->execute();
    $data = $stmnt->fetch();
?>

<section id="main-content">
<section class="wrapper">
  <h3><i class="fa fa-mapel"></i> DETAIL UJIAN </h3>
  <div class="row">
    <div class="col-md-12">
      <div class="content-panel content-table">
        <form class="form-add" method='post'action="tambahmapel.php">
            <table class='table table-bordered'>
                <tr>
                    <td>Nama Ujian</td>
                    <td>:</td>
                    <td><?php echo $data['testname'] ?> </td>
                </tr>
                <tr>
                    <td>Waktu Valid</td>
                    <td>:</td>
                    <td><?php echo $data['testfrom'] ?> s.d. <?php echo $data['testto'] ?></td>
                </tr>
                <tr>
                    <td>Nama Mata Pelajaran</td>
                    <td>:</td>
                    <td><?php echo $data['subname'] ?></td>
                </tr>
                <tr>
                    <td>Siswa yang Mengikuti</td>
                    <td>:</td>
                    <td><?php echo $data['attemptedstudents'] ?></td>
                </tr>
            </table>
            <br>
            <br>
            <h3>Daftar Siswa yang Mengikuti</h3>
            <br>
            <table id='detailujian' class='table table-striped table-bordered' cellspacing='0' width='100%'> 
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nomor Induk Siswa</th>
                        <th>Nama</th>
                        <th>Jumlah Soal Terjawab Benar</th>
                        <th>Nilai</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                    <th>No</th>
                        <th>Nomor Induk Siswa</th>
                        <th>Nama</th>
                        <th>Jumlah Soal Terjawab Benar</th>
                        <th>Nilai</th>
                    </tr>
                </tfoot>
                <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </form>
      </div>
    </div>
  </div>
</section>
</section>
<script>
  $(document).ready(function(){
    $("#detailujian").DataTable();
  });
</script>
<?php
    include('footer.php'); 
?>
