<?php
  include('header.php');
  $id=$_GET['id'];
?>
<section id="main-content">
  <section class="wrapper">
    <h3><i class="fa fa-mapel"></i> Daftar Pertanyaan</h3>
      <div class="row">
        <div class="col-md-12">
          <div class="content-panel content-table">
            <div class="action-button pull-right">
                <a href='tambahpertanyaan.php?id=<?php echo $id;?>' class="btn btn-large btn-info button-add"><i class="glyphicon glyphicon-plus"></i> &nbsp; Tambah Pertanyaan</a>
            </div>
            <hr>
            <?php
              require_once '../db.php';
              $sql="SELECT testid,qnid,question,correctanswer,marks,optiona,optionb,optionc,optiond
                    FROM question
                    WHERE testid = $id
                    ORDER BY qnid";
              $stmnt = $dbh->prepare($sql);
              $stmnt->execute();
              if ($stmnt->rowCount() == 0) {
                  echo'Tidak Ada Data';
              } else {
                echo
                  "<table id='pertanyaan' class='table table-striped table-bordered' cellspacing='0' width='100%'>
                    <thead>
                      <tr>
                         <th>No</th>
                         <th>Pertanyaan</th>
                         <th>Jawaban Benar</th>
                         <th>Bobot Nilai</th>
                         <th class='action' align='center'>Aksi</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <th>No</th>
                        <th>Pertanyaan</th>
                        <th>Jawaban Benar</th>
                        <th>Bobot Nilai</th>
                        <th class='action' align='center'>Aksi</th>
                      </tr>
                    </tfoot>
                    <tbody>";
                        while ($row = $stmnt->fetch()){
    												$qnid=$row['qnid'];
                            $question=$row['question'];
                            $correctanswer=$row['correctanswer'];
                            $optiona=$row['optiona'];
                            $optionb=$row['optionb'];
                            $optionc=$row['optionc'];
                            $optiond=$row['optiond'];
    												$marks=$row['marks'];
    												echo
                          "<tr>
                              <td>$qnid</td>
                              <td>$question</td>
                              <td>
                                $correctanswer
                              </td>
                              <td>$marks</td>
                              <td>
                                  <a href='editpertanyaan.php?id=" . $row['qnid'] . "' class='btn btn-xs btn-warning' title='Ubah'><i class='fa fa-edit'></i></a>
                                  <a href='deletepertanyaan.php?id=" . $row['qnid'] . "' class='btn btn-xs btn-danger' title='Hapus'><i class='fa fa-trash-o'></i></a>
                              </td>
                          </tr>";
                        }
                        echo
                    "</tbody>
                 </table>";
               }
               ?>
            </div><! --/content-panel -->
         </div><!-- /col-md-12 -->
      </div><!-- row -->
  </section>
</section>

<script>
  $(document).ready(function(){
    $("#pertanyaan").DataTable();
  });
</script>
<?php
  include('footer.php'); 
?>
