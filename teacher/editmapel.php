<?php
  include('header.php');
?>
<?php
  include 'ceklogin.php';
  $subid = filter_input(INPUT_POST,"subid");
  $subname = filter_input(INPUT_POST, "subname");
  $classid = filter_input(INPUT_POST, "classid");
  $subdesc = filter_input(INPUT_POST, "subdesc");
  $idteach = filter_input(INPUT_POST, "tcid");
  require_once '../db.php';
  $id=$_GET['id'];
  $stmnt = $dbh->prepare("SELECT * from subject WHERE subid = '$id'");
  $stmnt->execute();
  $data = $stmnt->fetch();
?>

<section id="main-content">
<section class="wrapper">
  <h3><i class="fa fa-mapel"></i> UBAH MATA PELAJARAN </h3>
  <div class="row">
    <div class="col-md-12">
      <div class="content-panel content-table">
        <form class="form-add" method='post'action="editmapelf.php?id=<?php echo $id ?>">
              <table class='table table-bordered'>
                  <tr>
                      <td>Kode Mata Pelajaran</td>
                      <td><input type='text' name='subid' class='form-control' value="<?php echo $data['subid'] ?>" readonly></td>
                  </tr>
                  <tr>
                      <td>Nama Mata Pelajaran</td>
                      <td><input type='text' name='subname' class='form-control' value="<?php echo $data['subname'] ?>" required></td>
                  </tr>
                  <tr>
                      <td>Kelas</td>
                      <td>
                        <select id="level" class="form-control" name="classid" value="<?php echo $data['classid'] ?>" required>
                            <option value="">Silahkan Pilih Kelas</option>
                            <?php
                              require_once '../db.php';
                              $sql="SELECT * FROM `class` ";
                              $stmnt = $dbh->prepare($sql);
                              $stmnt->execute();
                              while ($class = $stmnt->fetch()){
                                $value="";
                                $classid=$class['classid'];
                                $classname=$class['classname'];
                                if($classid == $data['classid']){
                                  $value="selected";
                                }
                                echo "<option value='$classid' ".$value.">$classname</option>";
                              }
                            ?>
                        </select>
                      </td>
                  </tr>
                  <tr>
                      <td>Deskripsi Mata Pelajaran</td>
                      <td><textarea type='text' name='subdesc' class='form-control' required><?php echo $data['subdesc'] ?> </textarea></td>
                  </tr>
                  <tr>
                      <td>Guru Pengampu</td>
                      <td>
                        <select id="level" class="form-control" name="tcid" value="<?php echo $data['tcid'] ?>" required>
                            <option value="">Silahkan Pilih Guru Pengampu</option>
                            <?php
                              require_once '../db.php';
                              $sql="SELECT * FROM `testconductor` ";
                              $stmnt = $dbh->prepare($sql);
                              $stmnt->execute();
                              while ($teacher = $stmnt->fetch()){
                                $value="";
                                $idteach=$teacher['tcid'];
                                $nama=$teacher['tcfullname'];
                                if($idteach == $data['tcid']){
                                  $value="selected";
                                }
                                echo "<option value='$idteach' ".$value.">$nama</option>";
                              }
                            ?>
                        </select>
                      </td>
                  </tr>
                  <tr>
                      <td colspan="2">
                          <button type="submit" class="btn btn-primary" name="btn-update">
                              <span class="glyphicon glyphicon-edit"></span>  Update
                          </button>
                          <a href="mapel.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Batal</a>
                      </td>
                  </tr>

              </table>
          </form>

      </div>
    </div>
  </div>
</section>
</section>
<?php
include('footer.php'); ?>
