<?php 
session_start();
require_once '../db.php';
if (isset($_POST['submit'])) {
    if (isset($_POST['username']) && isset($_POST['password'])) {
        $username = $_POST['username'];
        $password = MD5($_POST['password']);
        $query = "SELECT * FROM `testconductor` WHERE tcusername='$username'";
        $stmnt = $dbh->prepare($query);
        $stmnt->execute();
        if ($stmnt->rowCount() == 0) {
            echo '<script type="text/javascript">alert("Username tidak ditemukan");window.location.replace("index.php");</script>';
        } else {
            $query = "SELECT * FROM `testconductor` WHERE tcusername='$username' and tcpassword='$password'";
            $stmnt = $dbh->prepare($query);
            $stmnt->execute();
            if ($stmnt->rowCount() == 0) {
                echo "<script type='text/javascript'>alert('Password anda salah'); window.location.replace('index.php');</script>";
            } else {
                while ($row = $stmnt->fetch()) {
                    $_SESSION['tcusername'] = $row['tcusername'];
                    echo '<script>alert("Berhasil Masuk");window.location.replace("dashboard.php");</script>';
                }
            }
        }
    } else {
        header("Location: index.php");
    }
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <title>GURU - E-LEARNING TAMAN SISWA 2 JAKARTA</title>
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="../assets/css/style.css" rel="stylesheet">
    <link href="../assets/css/style-responsive.css" rel="stylesheet">
    <link rel="icon" href="../assets/img/ui-sam.png" sizes="16x16">
  </head>
  <body background="../assets/img/aa.jpg">
	  <div id="login-page">
	  	<div class="container">
		      <form class="form-login" id="form-guru" action="index.php" method="post">
		        <h2 class="form-login-heading">MASUK</h2>
		        <div class="login-wrap">
		            <input type="text" class="form-control" name="username" placeholder="Nomor Induk Pegawai" autofocus>
		            <br>
		            <input type="password" class="form-control" name="password" placeholder="Password">
		            <label class="checkbox">
		                <span class="pull-right">
		                    <a data-toggle="modal" href="index.php#myModal"> Lupa Password?</a>

		                </span>
		            </label>
		            <button type="submit" name="submit" form="form-guru" class="btn btn-theme btn-block" name="btn-login"><i class="fa fa-lock"></i> MASUK</button>
		            <hr>
		        </div>
		          <!-- Modal -->
		          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
		              <div class="modal-dialog">
		                  <div class="modal-content">
		                      <div class="modal-header">
		                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                          <h4 class="modal-title">Lupa Password ?</h4>
		                      </div>
		                      <div class="modal-body">
		                          <p>Masukkan email anda.</p>
		                          <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">

		                      </div>
		                      <div class="modal-footer">
		                          <button data-dismiss="modal" class="btn btn-default" type="button">Batal</button>
		                          <button class="btn btn-theme" type="button">Kirim</button>
		                      </div>
		                  </div>
		              </div>
		          </div>
		      </form>
	  	</div>
	  </div>
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/js/jquery.backstretch.min.js"></script>
  </body>
</html>
