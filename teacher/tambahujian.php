<?php
  include('header.php');
?>
<?php
    include 'ceklogin.php';
    if (isset($_POST['btn-add'])) {
        $testname = filter_input(INPUT_POST, "testname");
        $classid = filter_input(INPUT_POST, "classid");
        $testdesc = filter_input(INPUT_POST, "testdesc");
        $subid = filter_input(INPUT_POST, "subid");
        $testfrom = filter_input(INPUT_POST,"testfrom");
        $testto = filter_input(INPUT_POST,"testto");
        $totalqn = filter_input(INPUT_POST,"totalqn");
        $duration = filter_input(INPUT_POST, "duration");
        $testcode = filter_input(INPUT_POST,"testcode");
        $tcid = filter_input(INPUT_POST, "tcid");
        $testdate = date('Y-m-d H:i:s');
          require_once '../db.php';
          $stmnt = $dbh->prepare("SELECT * FROM `test` where testid = '" . $testid . "'");
          $stmnt->execute();
          $row = $stmnt->rowCount();
          if ($row == 0) {
              $query = "INSERT INTO `test` ( testname, classid,  testdesc, testdate, subid, testfrom, testto,  duration, totalquestions, testcode, tcid)
                 VALUES('" . $testname . "','" . $classid . "', '" . $testdesc . "', '" . $testdate . "','".$subid."',
                 '" . $testfrom . "','" . $testto . "','" . $duration . "','" . $totalqn . "','" . $testcode . "',
                 '" . $tcid . "' )";
              $stmnt2 = $dbh->prepare($query);
              $stmnt2->execute();
              if ($stmnt2) {
                  echo "<script>alert('Berhasil Menambah Ujian');window.location.replace('ujian.php');</script>";
              } else {
                  echo "<script>alert('Gagal Menambahkan Ujian')</script>";
              }
          } else {
              echo '<script>alert("Ujian telah ada")</script>';
          }
      }
?>
<section id="main-content">
<section class="wrapper">
  <h3><i class="fa fa-mapel"></i> TAMBAH UJIAN </h3>
  <div class="row">
    <div class="col-md-12">
      <div class="content-panel content-table">
        <form class="form-add" method='post'action="">
              <table class='table table-bordered'>
                   <tr>
                      <td>Mata Pelajaran</td>
                      <td>
                        <select id="level" class="form-control" name="subid" required>
                            <option value="">Silahkan Pilih Mata Pelajaran</option>
                            <?php
                              require_once '../db.php';
                              $sql="SELECT * FROM `subject` ";
                              $stmnt = $dbh->prepare($sql);
                              $stmnt->execute();
                              while ($subject = $stmnt->fetch()){
                                $subid=$subject['subid'];
                                $subname=$subject['subname'];
                                echo "<option value='$subid'>$subname</option>";
                              }
                            ?>
                        </select>
                      </td>
                  </tr>
                  <tr>
                      <td>Kelas</td>
                      <td>
                        <select id="level" class="form-control" name="classid" required>
                            <option value="">Silahkan Pilih Kelas</option>
                            <?php
                              require_once '../db.php';
                              $sql="SELECT * FROM `class` ";
                              $stmnt = $dbh->prepare($sql);
                              $stmnt->execute();
                              while ($class = $stmnt->fetch()){
                                $classid=$class['classid'];
                                $classname=$class['classname'];
                                echo "<option value='$classid'>$classname</option>";
                              }
                            ?>
                        </select>
                      </td>
                  </tr>
                  <tr>
                      <td>Nama Ujian</td>
                      <td><input type='text' name='testname' class='form-control' value="" required></td>
                  </tr>
                  <tr>
                      <td>Deskripsi Ujian</td>
                      <td><textarea type='text' name='testdesc' class='form-control' value='' required></textarea></td>
                  </tr>
                  <tr>
                      <td>Total Pertanyaan</td>
                      <td><input type='number' name='totalqn' class='form-control' value='' required></td>
                  </tr>
                  <tr>
                      <td>Durasi (dalam menit)</td>
                      <td><input type='number' name='duration' class='form-control' value='' required></td>
                  </tr>
                  <tr>
                      <td>Waktu Tes Dari</td>
                      <td><input type='date' id="testfrom" name='testfrom' class='form-control' value='' required></td>
                  </tr>
                  <tr>
                      <td>Waktu Akhir Tes</td>
                      <td><input type='date' id="testto" name='testto' class='form-control' value='' required></td>
                  </tr>
                  <tr>
                      <td>Kode Tes</td>
                      <td><input type='number' name='testcode' class='form-control' value='' required></td>
                  </tr>
                  <tr>
                      <td>Guru Pengampu</td>
                      <td>
                        <select id="level" class="form-control" name="tcid" required>
                            <option value="">Silahkan Pilih Guru Pengampu</option>
                            <?php
                              require_once '../db.php';
                              $sql="SELECT * FROM `testconductor` ";
                              $stmnt = $dbh->prepare($sql);
                              $stmnt->execute();
                              while ($teacher = $stmnt->fetch()){
                                $tcid=$teacher['tcid'];
                                $tcname=$teacher['tcfullname'];
                                echo "<option value='$tcid'>$tcname</option>";
                              }
                            ?>
                        </select>
                      </td>
                  </tr>
                  <tr>
                      <td colspan="2">
                          <button type="submit" class="btn btn-primary" name="btn-add">
                              <span class="glyphicon glyphicon-edit"></span>  Tambah
                          </button>
                          <a href="#" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Batal</a>
                      </td>
                  </tr>

              </table>
          </form>

      </div>
    </div>
  </div>
</section>
</section>
<?php
include('footer.php'); ?>
