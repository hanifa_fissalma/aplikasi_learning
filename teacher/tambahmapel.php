<?php
  include('header.php');
?>
<?php
include 'ceklogin.php';
if (isset($_POST['btn-add'])) {
  $classid = filter_input(INPUT_POST, "classid");
  $subname = filter_input(INPUT_POST, "subname");
  $subdesc = filter_input(INPUT_POST, "subdesc");
  $idteach = filter_input(INPUT_POST, "tcid");
    require_once '../db.php';
    $stmnt = $dbh->prepare("SELECT * FROM `subject` where subid = '" . $subid . "'");
    $stmnt->execute();
    $row = $stmnt->rowCount();
    if ($row == 0) {
        $query = "INSERT INTO `subject` (classid,subname,subdesc,tcid)
           VALUES('" . $classid . "','" . $subname . "', '" . $subdesc . "','".$idteach."')";
        $stmnt2 = $dbh->prepare($query);
        $stmnt2->execute();
        if ($stmnt2) {
            echo "<script>alert('Berhasil Menambah Mata Pelajaran');window.location.replace('mapel.php');</script>";
        } else {
            echo "<script>alert('Gagal Menambahkan Mata Pelajaran')</script>";
        }
    } else {
        echo '<script>alert("Mata Pelajaran telah ada")</script>';
    }
}
?>
<section id="main-content">
<section class="wrapper">
  <h3><i class="fa fa-mapel"></i> TAMBAH MATA PELAJARAN </h3>
  <div class="row">
    <div class="col-md-12">
      <div class="content-panel content-table">
        <form class="form-add" method='post'action="tambahmapel.php">
              <table class='table table-bordered'>
                  <tr>
                      <td>Nama Mata Pelajaran</td>
                      <td><input type='text' name='subname' class='form-control' value="" required></td>
                  </tr>
                  <tr>
                      <td>Kelas</td>
                      <td>
                        <select id="level" class="form-control" name="classid" required>
                            <option value="">Silahkan Pilih Kelas</option>
                            <?php
                              require_once '../db.php';
                              $sql="SELECT * FROM `class` ";
                              $stmnt = $dbh->prepare($sql);
                              $stmnt->execute();
                              while ($class = $stmnt->fetch()){
                                $classid=$class['classid'];
                                $classname=$class['classname'];
                                echo "<option value='$classid'>$classname</option>";
                              }
                            ?>
                        </select>
                      </td>
                  </tr>
                  <tr>
                      <td>Deskripsi Mata Pelajaran</td>
                      <td><textarea type='text' name='subdesc' class='form-control' value="" required></textarea></td>
                  </tr>
                  <tr>
                      <td>Guru Pengampu</td>
                      <td>
                        <select id="level" class="form-control" name="tcid" required>
                            <option value="">Silahkan Pilih Guru Pengampu</option>
                            <?php
                              require_once '../db.php';
                              $sql="SELECT * FROM `testconductor` ";
                              $stmnt = $dbh->prepare($sql);
                              $stmnt->execute();
                              while ($teacher = $stmnt->fetch()){
                                $idteach=$teacher['tcid'];
                                $nama=$teacher['tcfullname'];
                                echo "<option value='$idteach'>$nama</option>";
                              }
                            ?>
                        </select>
                      </td>
                  </tr>
                  <tr>
                      <td colspan="2">
                          <button type="submit" class="btn btn-primary" name="btn-add">
                              <span class="glyphicon glyphicon-edit"></span>  Tambah
                          </button>
                          <a href="#" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Batal</a>
                      </td>
                  </tr>

              </table>
          </form>

      </div>
    </div>
  </div>
</section>
</section>
<?php
include('footer.php'); ?>
