<?php
  include 'ceklogin.php';
?>
<?php
include('header.php');
?>
<section id="main-content">
<section class="wrapper">
  <h3><i class="fa fa-mapel"></i> LIHAT HASIL UJIAN</h3>
  <div class="row">
    <div class="col-md-12">
      <div class="content-panel content-table">
        
        <table id="lihathasil" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Tanggal dan Waktu Ujian</th>
                <th>Nama Ujian</th>
                <th>Jumlah Soal</th>
                <th>Jumlah Jawaban Benar</th>
                <th>Presentase</th>
                <th class="action" align="center">Detail</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>Tanggal dan Waktu Ujian</th>
                <th>Nama Ujian</th>
                <th>Jumlah Soal</th>
                <th>Jumlah Jawaban Benar</th>
                <th>Presentase</th>
                <th class="action" align="center">Detail</th>
              </tr>
            </tfoot>
            <tbody>
              <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>
                      <a href="#" title="Lihat Detail" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
                  </td>
              </tr>
            </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
</section>
<script>
  $(document).ready(function() {
    $('#lihathasil').DataTable();
} );
</script>
<?php
include('footer.php'); ?>
