<?php
	include_once "db.php"; 
	include_once "ceklogin.php"; 
	include_once "header.php";
	$id=$_GET['id'];
	$username= $_SESSION['stdusername'];
	/* mengambil data ujian berdasarkan id_ujian yang dipilih seperti nama ujian, waktu ujian dll */
	$query = "SELECT test.testname, test.testdesc, subject.subname, class.classname, test.duration, test.totalquestions
			  FROM subject, test, class
			  WHERE subject.subid=test.subid AND class.classid=test.classid AND test.testid=$id";
	$stmnt = $dbh->prepare($query);
	$stmnt->execute();
	$data = $stmnt->fetch();
	/* set session untuk mengecek waktu yang telah berjalan */
	if(isset($_SESSION["mulai_".$id])){
		$telah_berlalu = time() - $_SESSION["mulai_".$id];
		}
	else {
		$_SESSION["mulai_".$id] = time();
		$telah_berlalu = 0;
		}	
?>
<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-mapel"></i> UJIAN</h3>
			<hr>
			<script type="text/javascript" src="assets/js/jquery.countdown.js"></script>
			<div class="content-panel content-table">
				<div class="block-content collapse in">
					<div class="span12">
						<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
							<tr>
								<td width="150">Username</td>
								<td width="300"><?php echo $_SESSION['stdusername']; ?></td>
								<td width="150">Kelas</td>
								<td width="300"><?php echo $data['classname']; ?></td>
							</tr>
							<tr>
								<td>Mata Pelajaran</td>
								<td><?php echo ucwords($data['subname'])." ( ".ucwords($data['testname']).")" ?></td>
								<td>Jumlah Soal</td>
								<td><?php echo ucwords($data['totalquestions']) ?> Soal</td>
							</tr>
							<tr>
								<td>Keterangan</td>
								<td><?php echo ucwords($data['testdesc'])." " ?></td>
								<td>Waktu</td>
								<td>00 : 00 : 00</td>
							</tr>
						</table>
					</div>
				</div>
				tes tes
			</div>
		</div>
	</section>
</section>

<?php
	include_once "footer.php"
?>